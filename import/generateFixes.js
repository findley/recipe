var http = require('http');
var fs   = require('fs');

var recipes = JSON.parse(fs.readFileSync('cleanrecipes.json', 'utf8'));
var broken = JSON.parse(fs.readFileSync('broken.json', 'utf8'));

console.log('Found %d broken recipies.', broken.length);

let matches = 0;

let fixes = [];

for (let i = 0; i < broken.length; i++) {
    let matchCount = 0;
    for (let j = 0; j < recipes.length; j++) {
        let rname = recipes[j].name.toLowerCase();
        let bname = broken[i].name.toLowerCase();
        let rauthor = recipes[j].author.toLowerCase();
        let bauthor = broken[i].author.toLowerCase();
        if (rname == bname && rauthor == bauthor) {
            if (matchCount == 0)
                matches++;
            matchCount++;
            recipes[j].name = broken[i].name;
            recipes[j].author = broken[i].author;
            fixes.push({
                existing: broken[i],
                replacement: recipes[j]
            });
        }
    }
    if (matchCount > 1) {
        console.log('Found duplicate %d matche(s) for %s.', matchCount, broken[i].name);
    }
}

console.log('Found %d matches for broken recipes.', matches);
console.log(JSON.stringify(fixes, null, 4));
