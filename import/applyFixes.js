let http = require('http');
let fs   = require('fs');
let async = require('asyncawait/async');
let await = require('asyncawait/await');
let rp = require('request-promise');

let fixes = JSON.parse(fs.readFileSync('fixes.json', 'utf8'));

console.log('Applying fixes to %d recipes', fixes.length);

let process = async(() => {
    for (let i = 0; i < fixes.length; i++) {
        let fix = fixes[i];
        console.log('Deleting original recipe: %s', fix.replacement.name);
        if (!await(deleteRecipe(fix.existing._id))) {
            console.log('Failed to delete %s - %s', fix.existing._id, fix.replacement.name);
        }
        console.log('Reinserting recipe: %s', fix.replacement.name);
        if (!await(postRecipe(fix.replacement))) {
            console.log('Failed to post %s - %s', fix.replacement.name, fix.replacement.author);
        }
    }
});

function deleteRecipe(id) {
    return new Promise((resolve, reject) => {
        rp({
            uri: 'http://findley.recipes/api/recipes/' + id,
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0Nzk4NzQ4NjguNDh9.RBJCAi4_gStn0YFACEpnlsrMbCy1MOl-kaz72oVjLms'
            }
        })
        .then(() => {
            resolve(true);
        })
        .catch((err) => {
            resolve(false);
        });
    });
}

function postRecipe(recipe) {
    return new Promise((resolve, reject) => {
        rp({
            uri: 'http://findley.recipes/api/recipes',
            method: 'POST',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0Nzk4NzQ4NjguNDh9.RBJCAi4_gStn0YFACEpnlsrMbCy1MOl-kaz72oVjLms'
            },
            body: recipe,
            json: true
        })
        .then(() => {
            resolve(true);
        })
        .catch((err) => {
            resolve(false);
        });
    });
}

process();
