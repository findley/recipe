FROM node:6-slim

# Setup
RUN mkdir -p /usr/src/recipe
RUN mkdir -p /usr/src/recipe/v2
RUN mkdir -p /usr/src/recipe/public
RUN mkdir -p /data/v2
WORKDIR /usr/src/recipe

# Install server dependencies
COPY package.json /usr/src/recipe
RUN npm install

# Install legacy front-end dependencies
COPY public/package.json /usr/src/recipe/public
RUN cd /usr/src/recipe/public && npm install
RUN npm install -g grunt-cli

# Install front-end dependencies
COPY v2/package.json v2/package-lock.json /usr/src/recipe/v2/
RUN cd /usr/src/recipe/v2 && npm install

COPY . /usr/src/recipe

RUN cd /usr/src/recipe/public && grunt
RUN cd /usr/src/recipe/v2 && npm run build

EXPOSE 80 443
CMD cp -rf /usr/src/recipe/v2/build /data/v2 && npm start
