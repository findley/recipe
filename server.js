// Command line flags
var flags    = require('flags');
flags.defineInteger('port', 80, 'Port the app will run on.');
flags.defineString('meta', 'meta.json', 'File where metadata is saved');
flags.parse();

var restify  = require('restify');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var handlers = require('./handlers');
var middleware = require('./middleware');
const corsMiddleware = require('restify-cors-middleware');

// Connect to mongodb
mongoose.connect('mongodb://mongo/recipe');
mongoose.connection.on('error', e => {
    console.log('Could not connect to mongodb');
    console.log(e);
    setTimeout(() => {
        mongoose.connect('mongodb://mongo/recipe');
    }, 5000);
});
mongoose.connection.on('disconnected', () => {
    console.log('disconnected from mongodb');
});

// Create a restify server
var server = restify.createServer({
    name: 'Findley Recipe API'
});

// Attach middleware
const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['Authorization'],
    exposeHeaders: ['Authorization'],
});

server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.use(function(req, res, next) {
    if (res._responseTime) return next();
    res._responseTime = true;
    var start = new Date();
    res.on('header', function() {
        var duration = new Date - start;
        res.setHeader('Response-Time', duration + 'ms');
    });
    next();
});

server.get('/api/dash', handlers.dash.get);

// Recipe resource
server.post ('/api/recipes'               , middleware.authenticated, handlers.recipes.create);
server.get  ('/api/recipes/meta'          , handlers.recipes.meta);
server.get  ('/api/recipes/typeahead/:pre', handlers.recipes.typeahead);
server.patch('/api/recipes/:id'           , middleware.authenticated, handlers.recipes.update);
server.del  ('/api/recipes/:id'           , middleware.authenticated, handlers.recipes.delete);
server.get  ('/api/recipes/:id'           , handlers.recipes.get);
server.get  ('/api/recipes'               , handlers.recipes.search);
server.get  ('/api/recipes/:id/print'     , handlers.recipes.print);

server.post ('/api/authenticate',   handlers.authenticate.authenticate);
server.post ('/api/changepassword', middleware.authenticated, handlers.authenticate.changePassword);

// Start server
server.listen(flags.get('port'), function() {
    console.log('%s listening at %s', server.name, server.url);
});
