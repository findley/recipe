import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {Typography, Avatar} from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';

const styles = theme => ({
    step: {
        marginTop: theme.spacing.unit * 2,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    stepNumber: {
        marginRight: theme.spacing.unit * 2,
        width: 30,
        height: 30,
        backgroundColor: 'rgba(0,0,0,0)',
        color: theme.palette.primary.main,
        borderColor: theme.palette.primary.main,
        borderStyle: 'solid',
        borderWidth: 1
    },
    stepNumberComplete: {
        backgroundColor: theme.palette.primary.main,
        color: 'white'
    }
});

class InstructionStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = { complete: false };
    }

    toggleComplete() {
        this.setState(prevState => ({
            complete: !prevState.complete
        }));
    }

    render() {
        const {classes, showStepNumber} = this.props;
        const textColor = (this.state.complete && showStepNumber) ? 'textSecondary' : 'default';

        return (
            <div
                className={classes.step}
                onClick={this.toggleComplete.bind(this)}>
                {this.renderAvatar()}
                <Typography variant="body2" color={textColor}>{this.props.children}</Typography>
            </div>
        );
    }

    renderAvatar() {
        const {stepNumberComplete, stepNumber} = this.props.classes;
        const {complete} = this.state;
        const classes = classNames( stepNumber, {[stepNumberComplete]: complete});

        if (this.props.showStepNumber) {
            return (
                <Avatar
                    className={classes}>
                    {this.state.complete ? (<CheckIcon />) : this.props.number}
                </Avatar>
            );
        }
    }
}

export default withStyles(styles)(InstructionStep);
