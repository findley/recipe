import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {Typography} from '@material-ui/core';

const Ingredient = props => {
    const {classes, ingredient} = props;

    return (
        <Typography variant="body1">
            <span className={classes.quantityText}>{ingredient.qty}</span>
            <span className={classes.unitText}>{ingredient.unit}</span>
            {ingredient.name}
        </Typography>
    );
};

const styles = theme => ({
    quantityText: {
        minWidth: 40,
        display: 'inline-block'
    },
    unitText: {
        minWidth: 40,
        display: 'inline-block'
    }
});

export default withStyles(styles)(Ingredient);
