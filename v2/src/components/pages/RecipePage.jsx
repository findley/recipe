import React from 'react';
import { Route, Switch } from 'react-router-dom';
import LinearProgress from '@material-ui/core/LinearProgress';
import Recipe from '../recipe/Recipe';
import RecipeEditor from '../recipe-editor/RecipeEditor';
import RecipePrint from '../recipe-print/RecipePrint';
import { saveRecipe, fetchRecipe } from '../../services/recipe';

export default class RecipePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            recipe: null,
            loading: true
        }

        this.handleCancelEdit = this.handleCancelEdit.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    render() {
        if (this.state.loading) {
            return (
                <LinearProgress />
            );
        }

        return (
            <Switch>
                <Route exact path="/recipes/:id" render={props => (
                    <Recipe recipe={this.state.recipe} />
                )} />
                <Route path="/recipes/:id/edit" render={props => (
                    <RecipeEditor onCancel={this.handleCancelEdit} recipe={this.state.recipe} onSave={this.handleSave} />
                )} />
                <Route path="/recipes/:id/print" render={props => (
                    <RecipePrint recipe={this.state.recipe} />
                )} />
            </Switch>
        );
    }

    componentDidMount() {
        this.fetchData(this.props.match.params.id);
    }

    fetchData(id) {
        const {history} = this.props;

        if (history.location.state !== undefined) {
            this.setState({recipe: history.location.state, loading: false})
        } else {
            fetchRecipe(id)
            .then(recipe => {
                this.setState({recipe: recipe, loading: false});
            })
            .catch(response => {
                if (response.status === 404) {
                    history.push('/404');
                }
            });
        }
    }

    handleCancelEdit() {
        const {history, match} = this.props;
        history.push(`/recipes/${match.params.id}`);
    }

    handleSave(recipe) {
        saveRecipe(recipe)
        .then(r => {
            const {history} = this.props;
            history.push(`/recipes/${recipe.linkName}`, recipe);
            this.fetchData(recipe.linkName);
        })
        .catch(e => alert('Failed to save recipe'));
    }
};
