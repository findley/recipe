import React from 'react';
import { IconButton } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import RefreshIcon from '@material-ui/icons/Refresh';
import RecipeList from '../recipe-list/RecipeList';
import { fetchDash } from '../../services/dashboard';

const styles = theme => ({
    recipeList: {
        marginBottom: theme.spacing.unit * 5
    }
});

class HomePage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            favorites: [],
            newest: [],
            loading: true,
            reloading: false
        };
    }

    render() {
        const { recipeList } = this.props.classes;
        const { loading } = this.state;

        return (
            <div>
                <RecipeList
                    loading={loading}
                    header="Random Favorites"
                    classes={{root: recipeList}}
                    recipes={this.state.favorites}
                >
                    <IconButton onClick={this.fetchData.bind(this)} disabled={this.state.reloading}>
                        <RefreshIcon />
                    </IconButton>
                </RecipeList>

                <RecipeList
                    loading={loading}
                    header="Random New"
                    classes={{root: recipeList}}
                    recipes={this.state.newest}
                />
            </div>
        );
    }

    fetchData() {
        const {history} = this.props;

        this.setState({reloading: true});

        fetchDash()
        .then(response => {
            history.replace('/', response);
            this.setState({...this.state, ...response, loading: false, reloading: false});
        });
    }

    componentDidMount() {
        const {location} = this.props;

        this.fetchData();
        if (location.state !== undefined) {
            this.setState({...this.state, ...location.state, loading: false})
        } else {
            this.fetchData();
        }
    }
}

export default withStyles(styles)(HomePage);
