import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {Paper, Typography, TextField, Button} from '@material-ui/core';
import { authenticate } from '../../services/authentication';
import actions from '../../state/actions';

const styles = theme => ({
    container: {
        display: 'flex',
        justifyContent: 'center'
    },
    root: {
        width: '100%',
        padding: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 4,
        maxWidth: 500
    },
    footer: {
        display: 'flex',
        justifyContent: 'flex-end'
    }
});

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            error: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        authenticate(this.state.password)
        .then(token => {
            this.props.dispatch(actions.login(token));
        })
        .catch(response => {
            if (response.status < 500) {
                this.setState({
                    error: 'Incorrect password'
                });
            } else {
                this.setState({
                    error: 'Server error'
                });
            }
        });
    }

    handleChange(event) {
        this.setState({
            password: event.target.value
        });
    }

    render() {
        const {classes} = this.props;

        return (
            <div className={classes.container}>
                <Paper className={classes.root}>
                    <form onSubmit={this.handleSubmit}>
                        <Typography variant="headline">Login</Typography>
                        <div>
                            <TextField
                                type="password"
                                value={this.state.password}
                                onChange={this.handleChange}
                                fullWidth={true}
                                name="password"
                                label="Password"
                                margin="normal"
                                error={this.state.error !== ''}
                                helperText={this.state.error}
                                required
                            />
                        </div>
                        <div className={classes.footer}>
                            <Button type="submit">Login</Button>
                        </div>
                    </form>
                </Paper>
            </div>
        );
    }
}

export default connect()(withStyles(styles)(LoginPage));
