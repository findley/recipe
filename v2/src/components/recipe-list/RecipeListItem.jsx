import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
    Hidden,
} from '@material-ui/core';
import RecipeTagList from '../utils/RecipeTagList';
import CategoryIcon from '../utils/CategoryIcon';

const RecipeListItem = props => {
    const {recipe} = props;

    return (
        <Link to={`/recipes/${recipe.linkName}`}>
            <ListItem className="list-item-recipe">
                    <CategoryIcon iconName={recipe.icon} />
                    <ListItemText primary={recipe.name} secondary={recipe.author} />
                    <Hidden implementation="css" xsDown>
                        <ListItemSecondaryAction>
                            <RecipeTagList tags={recipe.tags} link={false} />
                        </ListItemSecondaryAction>
                    </Hidden>
            </ListItem>
        </Link>
    );
};

RecipeListItem.propTypes = {
    recipe: PropTypes.object.isRequired
};

export default RecipeListItem;