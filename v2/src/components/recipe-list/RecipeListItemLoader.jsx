import React from 'react';
import {
    ListItem,
} from '@material-ui/core';
import Shimmer from '../utils/Shimmer';

const listTextStyle = {
    display: 'flex',
    justifyContent: 'space-around',
    flexDirection: 'column',
    height: '34px',
    marginLeft: '13px',
};

const RecipeListItemLoader = () => {
    const mainWidth = 200 + Math.floor(Math.random() * 120 - 60);
    const secondaryWidth = 130 + Math.floor(Math.random() * 50 - 25);
    return (
        <div style={{height: '68px'}}>
            <ListItem>
                <Shimmer width={40} height={40} radius={20} />
                <div style={listTextStyle}>
                    <Shimmer width={mainWidth} height={13} radius={4} />
                    <Shimmer width={secondaryWidth} height={10} radius={4} />
                </div>
            </ListItem>
        </div>
    );
};

export default RecipeListItemLoader;
