import './app-search-bar.css';
import React from 'react';
import { connect } from 'react-redux';
import SearchIcon from '@material-ui/icons/Search';
import actions from '../../state/actions';
import { parseSearch } from '../../services/search';

class AppSearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }

    handleChange(event) {
        const q = event.target.value;
        this.props.dispatch(actions.updateSearch(q));
    }

    handleFocus(event) {
        this.props.dispatch(actions.startSearch(this.props.q));
    }

    handleBlur(event) {
        if (event.target.value === '') {
            this.props.dispatch(actions.clearSearch());
        }
    }

    render() {
        return (
            <div className="appbar-search">
                <div className="appbar-search-icon">
                    <SearchIcon />
                </div>
                <span>
                    <input
                        ref={input => this.searchInput = input}
                        value={this.props.q}
                        placeholder="Search"
                        className="appbar-search-input"
                        autoComplete="off"
                        onChange={this.handleChange}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        spellCheck="false"
                    />
                </span>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    q: parseSearch(state.router.location.pathname).q
});

export default connect(mapStateToProps)(AppSearchBar);
