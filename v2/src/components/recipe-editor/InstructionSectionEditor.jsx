import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    TextField,
    Button,
} from '@material-ui/core';

const InstructionSectionEditor = class InstructionSectionEditor extends React.Component {
    render() {
        const {instructions, classes} = this.props;

        return (
            <div className={classes.root}>
                {this.renderNameInput()}
                <TextField
                    defaultValue={instructions.content}
                    label="Instructions"
                    multiline
                    margin="normal"
                    fullWidth={true}
                    onChange={event => this.props.onChange({content: event.target.value})}
                />
                <div className={classes.footer}>
                    {this.renderRemoveButton()}
                </div>
            </div>
        );
    }

    renderNameInput() {
        const {instructions, classes, removeable} = this.props;

        if (!removeable) {
            return;
        }

        return (
            <TextField
                fullWidth={true}
                defaultValue={instructions.name}
                className={classes.sectionNameInput}
                InputLabelProps={{className: classes.sectionNameLabel}}
                label="Instruction Section"
                margin="normal"
                onChange={event => this.props.onChange({name: event.target.value})}
            />
        );
    }

    renderRemoveButton() {
        const {removeable} = this.props;

        if (!removeable) {
            return;
        }

        return (
            <Button onClick={this.props.onRemove}>
                Remove Section
            </Button>
        );
    }
};

const styles = theme => ({
    root: {
        border: 'solid #ababab 1px',
        borderRadius: 8,
        padding: 10,
        marginBottom: theme.spacing.unit * 3,
        marginTop: theme.spacing.unit * 2
    },
    sectionNameInput: {
        marginTop: -18
    },
    sectionNameLabel: {
        backgroundColor: 'white',
        padding: '0 4px'
    },
    footer: {
        display: 'flex',
        justifyContent: 'flex-end'
    }
});

export default withStyles(styles)(InstructionSectionEditor);
