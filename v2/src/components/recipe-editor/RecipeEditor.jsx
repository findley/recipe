import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Paper,
    Grid,
    TextField,
    Button,
    Typography,
} from '@material-ui/core';
import ChipInput from 'material-ui-chip-input';
import AddIcon from '@material-ui/icons/Add';
import * as ObjectId from 'objectid-browser';

import emptyRecipe from '../../services/emptyRecipe';

import IngredientSectionEditor from './IngredientSectionEditor';
import InstructionSectionEditor from './InstructionSectionEditor';
import CategoryIconPicker from './CategoryIconPicker';

class RecipeEditor extends React.Component {
    constructor(props) {
        super(props);

        let recipe;
        if (props.recipe !== undefined) {
            recipe = JSON.parse(JSON.stringify(props.recipe));
            if (!recipe.icon) {
                recipe.icon = 'meal';
            }
        } else {
            recipe = emptyRecipe();
        }

        this.state = recipe;

        this.handleChange = this.handleChange.bind(this);
        this.handleTagsChange = this.handleTagsChange.bind(this);
        this.addIngredientSection = this.addIngredientSection.bind(this);
        this.addInstructionSection = this.addInstructionSection.bind(this);
        this.removeIngredientSection = this.removeIngredientSection.bind(this);
        this.removeInstructionSection = this.removeInstructionSection.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleIngredientSectionChange(updatedSection, i) {
        this.setState(prevState => ({
            ingredients: prevState.ingredients.map((section, index) => {
                if (i !== index) {
                    return section;
                }

                return {...section, ...updatedSection};
            })
        }));
    }

    handleInstructionSectionChange(updatedSection, i) {
        this.setState(prevState => ({
            instructions: prevState.instructions.map((section, index) => {
                if (i !== index) {
                    return section;
                }

                return {...section, ...updatedSection};
            })
        }));
    }

    handleTagsChange(tags) {
        this.setState({
            tags
        });
    }

    addIngredientSection() {
        this.setState(prevState => ({
            ingredients: prevState.ingredients.concat({
                name: '',
                list: [],
                _id: (new ObjectId()).toString()
            })
        }));
    }

    addInstructionSection() {
        this.setState(prevState => ({
            instructions: prevState.instructions.concat({
                name: '',
                content: '',
                _id: (new ObjectId()).toString()
            })
        }));
    }

    removeIngredientSection(i) {
        this.setState(prevState => ({
            ingredients: [
                ...prevState.ingredients.splice(0, i),
                ...prevState.ingredients.splice(i+1)
            ]
        }));
    }

    removeInstructionSection(i) {
        this.setState(prevState => ({
            instructions: [
                ...prevState.instructions.splice(0, i),
                ...prevState.instructions.splice(i+1)
            ]
        }));
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.onSave(this.state);
    }

    render() {
        const {classes} = this.props;
        const recipe = this.state;
        const canRemoveIngredientSection = recipe.ingredients.length > 1;
        const canRemoveInstructionSection = recipe.instructions.length > 1;

        return (
            <form onSubmit={this.handleSubmit}>
                <Paper className={classes.root}>
                    <Grid container spacing={16}>
                        <Grid item md={6} xs={12}>
                            <TextField value={recipe.name} name="name" onChange={this.handleChange} fullWidth={true} label="Recipe Name" margin="normal" required />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <TextField value={recipe.author} name="author" onChange={this.handleChange} fullWidth={true} label="Author" margin="normal" required />
                        </Grid>
                    </Grid>
                    <Grid container spacing={16} alignItems="flex-end">
                        <Grid item md={10} xs={12}>
                            <div className={classes.categoryContainer}>
                                <ChipInput defaultValue={recipe.tags} onChange={this.handleTagsChange} fullWidth={true} label="Tags" margin="normal" newChipKeyCodes={[9, 13]} blurBehavior="add" />
                                <div className={classes.iconPickerContainer}>
                                    <CategoryIconPicker value={recipe.icon} onChange={v => this.setState({icon: v}) } />
                                </div>
                            </div>
                        </Grid>
                        <Grid item md={2} xs={12}>
                            <TextField value={recipe.yield} name="yield" onChange={this.handleChange} fullWidth={true} label="Servings" margin="normal" />
                        </Grid>
                    </Grid>

                    <div className={classes.ingredients}>
                        <Typography variant="headline">Ingredients</Typography>
                        {recipe.ingredients.map((ingredients, i) => (
                            <IngredientSectionEditor onRemove={() => this.removeIngredientSection(i)} removeable={canRemoveIngredientSection} onChange={section => this.handleIngredientSectionChange(section, i)} ingredients={ingredients} key={ingredients._id} />
                        ))}
                        <div className={classes.addSectionButtonContainer}>
                            <Button onClick={this.addIngredientSection}>
                                <AddIcon />
                                Add Section
                            </Button>
                        </div>
                    </div>

                    <div>
                        <Typography variant="headline">Instructions</Typography>
                        {recipe.instructions.map((instructions, i) => (
                            <InstructionSectionEditor onRemove={() => this.removeInstructionSection(i)} removeable={canRemoveInstructionSection} onChange={section => this.handleInstructionSectionChange(section, i)} instructions={instructions} key={instructions._id} />
                        ))}
                        <div className={classes.addSectionButtonContainer}>
                            <Button onClick={this.addInstructionSection}>
                                <AddIcon />
                                Add Section
                            </Button>
                        </div>
                    </div>

                    <div>
                        <Typography variant="headline">Notes</Typography>
                        <TextField
                            fullWidth={true}
                            name="notes"
                            multiline
                            value={recipe.notes}
                            label="Notes"
                            margin="normal"
                            onChange={this.handleChange}
                        />
                    </div>

                    <div className={classes.buttons}>
                        <Button variant="contained" type="button" onClick={this.props.onCancel}>
                            Cancel
                        </Button>
                        <Button variant="contained" type="submit" color="primary">
                            Save
                        </Button>
                    </div>
                </Paper>
            </form>
        );
    }
}

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 4
    },
    ingredients: {
        marginTop: theme.spacing.unit * 2
    },
    buttons: {
        marginTop: theme.spacing.unit * 5,
        display: 'flex',
        justifyContent: 'space-between'
    },
    addSectionButtonContainer: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    categoryContainer: {
        display: 'flex',
        alignItems: 'flex-end'
    },
    iconPickerContainer: {
        marginBottom: 12,
        marginLeft: 15
    }
});

export default withStyles(styles)(RecipeEditor);
