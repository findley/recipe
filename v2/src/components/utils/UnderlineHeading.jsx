import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {Typography} from '@material-ui/core';

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderBottomStyle: 'solid',
        borderBottomColor: theme.palette.primary.main
    },
    title: {
        display: 'inline-block'
    },
    subTitle: {
        display: 'inline-block',
        marginLeft: theme.spacing.unit * 2
    },
    rightMenu: {
        marginLeft: theme.spacing.unit
    }
});

const UnderlineHeading = props => (
    <div className={props.classes.root}>
        <div>
            <Typography variant="display1" color="textPrimary" className={props.classes.title}>{props.children}</Typography>
            <Typography variant="subheading" color="textSecondary" className={props.classes.subTitle}>
                {props.subTitle}
            </Typography>
        </div>
        <div className={props.classes.rightMenu}>
            {props.rightMenu ? props.rightMenu() : null}
        </div>
    </div>
);

export default withStyles(styles)(UnderlineHeading);
