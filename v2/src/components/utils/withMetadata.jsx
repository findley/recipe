import React from 'react';
import { getMetadata } from '../../services/metadata';

export function withMetadata(WrappedComponent) {
    return class extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                categories: []
            };
        }

        componentWillMount() {
            getMetadata()
            .then(metadata => {
                this.setState(metadata);
            });
        }

        render() {
            return (
                <WrappedComponent metadata={this.state} {...this.props} />
            );
        }
    }
}
