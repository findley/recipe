import React from 'react';
import { withStyles } from '@material-ui/core/styles';

export const iconMap = {
    lettuce: {
        backgroundColor: 'orange',
        img: '/icons/001-food.svg'
    },
    fruit: {
        backgroundColor: 'blue',
        img: '/icons/002-fruit.svg'
    },
    donut: {
        backgroundColor: 'green',
        img: '/icons/003-food-1.svg'
    },
    cheese: {
        backgroundColor: 'red',
        img: '/icons/004-food-2.svg'
    },
    cupcake: {
        backgroundColor: 'green',
        img: '/icons/005-food-3.svg'
    },
    redMeat: {
        backgroundColor: 'gray',
        img: '/icons/006-meat.svg'
    },
    fish: {
        backgroundColor: 'orange',
        img: '/icons/007-fish.svg'
    },
    cake: {
        backgroundColor: 'blue',
        img: '/icons/008-cake.svg'
    },
    ham: {
        backgroundColor: 'teal',
        img: '/icons/009-meat-1.svg'
    },
    meal: {
        backgroundColor: 'blue',
        img: '/icons/010-food-4.svg'
    },
    roll: {
        backgroundColor: 'blue',
        img: '/icons/011-bread.svg'
    },
    bread: {
        backgroundColor: 'blue',
        img: '/icons/012-bread-1.svg'
    },
    taco: {
        backgroundColor: 'blue',
        img: '/icons/013-food-5.svg'
    },
    kabab: {
        backgroundColor: 'green',
        img: '/icons/014-meat-2.svg'
    },
    mushroom: {
        backgroundColor: 'blue',
        img: '/icons/015-mushroom.svg'
    },
    corn: {
        backgroundColor: 'blue',
        img: '/icons/016-corn.svg'
    },
    burger: {
        backgroundColor: 'blue',
        img: '/icons/017-burger.svg'
    },
    potato: {
        backgroundColor: 'blue',
        img: '/icons/018-potato.svg'
    },
    sushi: {
        backgroundColor: 'green',
        img: '/icons/019-sushi.svg'
    },
    pepper: {
        backgroundColor: 'blue',
        img: '/icons/020-pepper.svg'
    },
    cajun: {
        backgroundColor: 'green',
        img: '/icons/021-soup.svg'
    },
    soup: {
        backgroundColor: 'green',
        img: '/icons/022-soup-1.svg'
    },
    pasta: {
        backgroundColor: 'blue',
        img: '/icons/023-pasta.svg'
    },
    egg: {
        backgroundColor: 'red',
        img: '/icons/fried-egg.svg'
    },
    chicken: {
        backgroundColor: 'red',
        img: '/icons/chicken.svg'
    }
}

const styles = theme => ({
    image: {
        width: 40,
        height: 40
    }
});

const CategoryIcon = props => {
    let {iconName} = props;

    if (!iconName || !iconMap[iconName]) {
        iconName = 'meal'
    }

    const icon = iconMap[iconName];

    return (
        <img className={props.classes.image} src={icon.img} alt={iconName} />
    );
};

export default withStyles(styles)(CategoryIcon);
