export function getMetadata() {
    return new Promise((resolve, reject) => {
        fetchLocalMetadata()
        .then(metadata => resolve(metadata))
        .catch(() => {
            fetchRemoteMetadata()
            .then(metadata => resolve(metadata));
        });
    });
}

function fetchRemoteMetadata() {
    return fetch(`http://138.68.37.159/api/recipes/meta`)
    .then(response => response.json())
    .then(metadata => {
        saveLocalMetadata(metadata);
        return metadata;
    });
}

function fetchLocalMetadata() {
    return new Promise((resolve, reject) => {
        const metadataString = window.localStorage.getItem('metadata');
        if (!metadataString) {
            return reject();
        }

        const m = JSON.parse(metadataString);
        if (Date.now() > m.exp) {
            return reject();
        }

        return resolve(m.metadata);
    });
}

const TTL = 86400000; // 1 day

function saveLocalMetadata(metadata) {
    const m = {
        metadata,
        exp: Date.now() + TTL
    };
    window.localStorage.setItem('metadata', JSON.stringify(m));
}
