import * as ObjectId from 'objectid-browser';

export default function emptyRecipe() {
    return {
        "icon":"meal",
        "name":"",
        "author":"",
        "category":"",
        "yield":"",
        "ingredients":[
            {
                "_id": (new ObjectId()).toString(),
                "name": "",
                "list":[]
            }
        ],
        "instructions":[
            {
                "name": '',
                "content":"",
                "_id": (new ObjectId()).toString(),
            }
        ],
        "tags":[],
        "notes": ''
    };
}
