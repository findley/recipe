import * as ObjectId from 'objectid-browser';

export default function emptyIngredient() {
    return {
        _id: (new ObjectId()).toString(),
        qty: '',
        unit: '',
        name: ''
    };
}
