import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

import './index.css';
import App from './components/layout/App';
import store from './state/store';

import registerServiceWorker from './registerServiceWorker';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#b71c1c',
            light: '#f05545',
            dark: '#7f0000',
            contrastText: '#ffffff'
        },
        secondary: {
            main: '#212121',
            light: '#484848',
            dark: '#000000',
            contrastText: '#ffffff'
        }
    }
});

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <App />
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
