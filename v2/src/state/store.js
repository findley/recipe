import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import reducer from './reducers';
import history from './history';
import { getLocalToken } from '../services/authentication';

const localToken = getLocalToken();
const initialState = {
    authenticated: localToken ? true : false,
    token: localToken
};

export default createStore(
    connectRouter(history)(reducer),
    initialState,
    compose(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    )
);
