export default function reducer(state, action) {
    switch (action.type) {
        case 'LOGIN':
            return login(state, action);
        case 'LOGOUT':
            return logout(state, action);
        default:
            return state;
    }
}

function login(state, action) {
    return {
        ...state,
        authenticated: true,
        token: action.token
    };
}

function logout(state, action) {
    return {
        ...state,
        authenticated: false,
        token: null
    };
}
