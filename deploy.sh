# Create a place for the docker-compose yaml file
mkdir -p /opt/recipe

# Copy the application service to systemd folder if it's not already there
cp ./systemd/recipe.service /etc/systemd/system/
systemctl daemon-reload

# Copy the docker-compose yaml file onto the host.
envsubst < docker-compose-prod.yml > /opt/recipe/recipe-compose.yml

# Pull the latest versions of all images used in the application.
docker-compose -f /opt/recipe/recipe-compose.yml pull

# Restart the service
systemctl restart recipe
