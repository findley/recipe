var fs = require('fs');
if (fs.existsSync('jwt_secret.key')) {
    module.exports = fs.readFileSync('jwt_secret.key');
} else {
    module.exports = 'secret';
}
